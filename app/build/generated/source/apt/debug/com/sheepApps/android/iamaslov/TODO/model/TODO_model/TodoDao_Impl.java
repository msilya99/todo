package com.sheepApps.android.iamaslov.TODO.model.TODO_model;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class TodoDao_Impl implements TodoDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfTodo;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfTodo;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfTodo;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAll;

  public TodoDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfTodo = new EntityInsertionAdapter<Todo>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR IGNORE INTO `todo`(`mTodoId`,`mTitleTodo`) VALUES (nullif(?, 0),?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Todo value) {
        stmt.bindLong(1, value.getTodoId());
        if (value.getTitleTodo() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getTitleTodo());
        }
      }
    };
    this.__deletionAdapterOfTodo = new EntityDeletionOrUpdateAdapter<Todo>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `todo` WHERE `mTodoId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Todo value) {
        stmt.bindLong(1, value.getTodoId());
      }
    };
    this.__updateAdapterOfTodo = new EntityDeletionOrUpdateAdapter<Todo>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR IGNORE `todo` SET `mTodoId` = ?,`mTitleTodo` = ? WHERE `mTodoId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Todo value) {
        stmt.bindLong(1, value.getTodoId());
        if (value.getTitleTodo() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getTitleTodo());
        }
        stmt.bindLong(3, value.getTodoId());
      }
    };
    this.__preparedStmtOfDeleteAll = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "Delete from todo ";
        return _query;
      }
    };
  }

  @Override
  public long insert(final Todo todo) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfTodo.insertAndReturnId(todo);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insert(final Todo... todo) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfTodo.insert(todo);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final Todo todo) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfTodo.handle(todo);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void update(final Todo todo) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfTodo.handle(todo);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteAll() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAll.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAll.release(_stmt);
    }
  }

  @Override
  public Todo findTodoById(final int id) {
    final String _sql = "SELECT * FROM todo WHERE mTodoId = ? LIMIT 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false);
    try {
      final int _cursorIndexOfMTodoId = CursorUtil.getColumnIndexOrThrow(_cursor, "mTodoId");
      final int _cursorIndexOfMTitleTodo = CursorUtil.getColumnIndexOrThrow(_cursor, "mTitleTodo");
      final Todo _result;
      if(_cursor.moveToFirst()) {
        final String _tmpMTitleTodo;
        _tmpMTitleTodo = _cursor.getString(_cursorIndexOfMTitleTodo);
        _result = new Todo(_tmpMTitleTodo);
        final int _tmpMTodoId;
        _tmpMTodoId = _cursor.getInt(_cursorIndexOfMTodoId);
        _result.setTodoId(_tmpMTodoId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Todo findTodoByName(final String mTitleTodo) {
    final String _sql = "SELECT * FROM todo WHERE mTitleTodo = ? LIMIT 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (mTitleTodo == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, mTitleTodo);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false);
    try {
      final int _cursorIndexOfMTodoId = CursorUtil.getColumnIndexOrThrow(_cursor, "mTodoId");
      final int _cursorIndexOfMTitleTodo = CursorUtil.getColumnIndexOrThrow(_cursor, "mTitleTodo");
      final Todo _result;
      if(_cursor.moveToFirst()) {
        final String _tmpMTitleTodo;
        _tmpMTitleTodo = _cursor.getString(_cursorIndexOfMTitleTodo);
        _result = new Todo(_tmpMTitleTodo);
        final int _tmpMTodoId;
        _tmpMTodoId = _cursor.getInt(_cursorIndexOfMTodoId);
        _result.setTodoId(_tmpMTodoId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public LiveData<List<Todo>> getAllTodo() {
    final String _sql = "Select * from todo Order by mTodoId ASC";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"todo"}, false, new Callable<List<Todo>>() {
      @Override
      public List<Todo> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfMTodoId = CursorUtil.getColumnIndexOrThrow(_cursor, "mTodoId");
          final int _cursorIndexOfMTitleTodo = CursorUtil.getColumnIndexOrThrow(_cursor, "mTitleTodo");
          final List<Todo> _result = new ArrayList<Todo>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Todo _item;
            final String _tmpMTitleTodo;
            _tmpMTitleTodo = _cursor.getString(_cursorIndexOfMTitleTodo);
            _item = new Todo(_tmpMTitleTodo);
            final int _tmpMTodoId;
            _tmpMTodoId = _cursor.getInt(_cursorIndexOfMTodoId);
            _item.setTodoId(_tmpMTodoId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public int findLastId() {
    final String _sql = "SELECT MAX(mTodoId) FROM todo ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false);
    try {
      final int _result;
      if(_cursor.moveToFirst()) {
        _result = _cursor.getInt(0);
      } else {
        _result = 0;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
