package com.sheepApps.android.iamaslov.TODO.model.TODO_model;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class SingleTodoDao_Impl implements SingleTodoDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfSingleTodoItem;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfSingleTodoItem;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfSingleTodoItem;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllSingleItems;

  public SingleTodoDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfSingleTodoItem = new EntityInsertionAdapter<SingleTodoItem>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR IGNORE INTO `singleItemTodo`(`mSingleItemId`,`mSingleItemTitle`,`mTodoSingleId`) VALUES (nullif(?, 0),?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, SingleTodoItem value) {
        stmt.bindLong(1, value.getSingleItemId());
        if (value.getSingleItemTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getSingleItemTitle());
        }
        stmt.bindLong(3, value.getTodoSingleId());
      }
    };
    this.__deletionAdapterOfSingleTodoItem = new EntityDeletionOrUpdateAdapter<SingleTodoItem>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `singleItemTodo` WHERE `mSingleItemId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, SingleTodoItem value) {
        stmt.bindLong(1, value.getSingleItemId());
      }
    };
    this.__updateAdapterOfSingleTodoItem = new EntityDeletionOrUpdateAdapter<SingleTodoItem>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR IGNORE `singleItemTodo` SET `mSingleItemId` = ?,`mSingleItemTitle` = ?,`mTodoSingleId` = ? WHERE `mSingleItemId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, SingleTodoItem value) {
        stmt.bindLong(1, value.getSingleItemId());
        if (value.getSingleItemTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getSingleItemTitle());
        }
        stmt.bindLong(3, value.getTodoSingleId());
        stmt.bindLong(4, value.getSingleItemId());
      }
    };
    this.__preparedStmtOfDeleteAllSingleItems = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "Delete from singleItemTodo ";
        return _query;
      }
    };
  }

  @Override
  public void insertSingleItem(final SingleTodoItem singleTodoItem) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfSingleTodoItem.insert(singleTodoItem);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertSingleItem(final SingleTodoItem... singleTodoItem) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfSingleTodoItem.insert(singleTodoItem);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteSingleItem(final SingleTodoItem singleTodoItem) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfSingleTodoItem.handle(singleTodoItem);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateSingleItem(final SingleTodoItem singleTodoItem) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfSingleTodoItem.handle(singleTodoItem);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteAllSingleItems() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllSingleItems.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllSingleItems.release(_stmt);
    }
  }

  @Override
  public LiveData<List<SingleTodoItem>> getAllSingleTodo() {
    final String _sql = "Select * from singleItemTodo Order by mSingleItemId ASC ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"singleItemTodo"}, false, new Callable<List<SingleTodoItem>>() {
      @Override
      public List<SingleTodoItem> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfMSingleItemId = CursorUtil.getColumnIndexOrThrow(_cursor, "mSingleItemId");
          final int _cursorIndexOfMSingleItemTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "mSingleItemTitle");
          final int _cursorIndexOfMTodoSingleId = CursorUtil.getColumnIndexOrThrow(_cursor, "mTodoSingleId");
          final List<SingleTodoItem> _result = new ArrayList<SingleTodoItem>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final SingleTodoItem _item;
            final String _tmpMSingleItemTitle;
            _tmpMSingleItemTitle = _cursor.getString(_cursorIndexOfMSingleItemTitle);
            final int _tmpMTodoSingleId;
            _tmpMTodoSingleId = _cursor.getInt(_cursorIndexOfMTodoSingleId);
            _item = new SingleTodoItem(_tmpMSingleItemTitle,_tmpMTodoSingleId);
            final int _tmpMSingleItemId;
            _tmpMSingleItemId = _cursor.getInt(_cursorIndexOfMSingleItemId);
            _item.setSingleItemId(_tmpMSingleItemId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public SingleTodoItem findSingleTodoByTitle(final String title) {
    final String _sql = "SELECT * FROM singleItemTodo WHERE mSingleItemTitle = ? ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (title == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, title);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false);
    try {
      final int _cursorIndexOfMSingleItemId = CursorUtil.getColumnIndexOrThrow(_cursor, "mSingleItemId");
      final int _cursorIndexOfMSingleItemTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "mSingleItemTitle");
      final int _cursorIndexOfMTodoSingleId = CursorUtil.getColumnIndexOrThrow(_cursor, "mTodoSingleId");
      final SingleTodoItem _result;
      if(_cursor.moveToFirst()) {
        final String _tmpMSingleItemTitle;
        _tmpMSingleItemTitle = _cursor.getString(_cursorIndexOfMSingleItemTitle);
        final int _tmpMTodoSingleId;
        _tmpMTodoSingleId = _cursor.getInt(_cursorIndexOfMTodoSingleId);
        _result = new SingleTodoItem(_tmpMSingleItemTitle,_tmpMTodoSingleId);
        final int _tmpMSingleItemId;
        _tmpMSingleItemId = _cursor.getInt(_cursorIndexOfMSingleItemId);
        _result.setSingleItemId(_tmpMSingleItemId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<SingleTodoItem> findSingleTodoById(final int todoId) {
    final String _sql = "SELECT * FROM singleItemTodo WHERE mTodoSingleId = ? ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, todoId);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false);
    try {
      final int _cursorIndexOfMSingleItemId = CursorUtil.getColumnIndexOrThrow(_cursor, "mSingleItemId");
      final int _cursorIndexOfMSingleItemTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "mSingleItemTitle");
      final int _cursorIndexOfMTodoSingleId = CursorUtil.getColumnIndexOrThrow(_cursor, "mTodoSingleId");
      final List<SingleTodoItem> _result = new ArrayList<SingleTodoItem>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final SingleTodoItem _item;
        final String _tmpMSingleItemTitle;
        _tmpMSingleItemTitle = _cursor.getString(_cursorIndexOfMSingleItemTitle);
        final int _tmpMTodoSingleId;
        _tmpMTodoSingleId = _cursor.getInt(_cursorIndexOfMTodoSingleId);
        _item = new SingleTodoItem(_tmpMSingleItemTitle,_tmpMTodoSingleId);
        final int _tmpMSingleItemId;
        _tmpMSingleItemId = _cursor.getInt(_cursorIndexOfMSingleItemId);
        _item.setSingleItemId(_tmpMSingleItemId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
