package com.sheepApps.android.iamaslov.TODO.model;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import com.sheepApps.android.iamaslov.TODO.model.Note_model.NoteDao;
import com.sheepApps.android.iamaslov.TODO.model.Note_model.NoteDao_Impl;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoDao;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoDao_Impl;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.TodoDao;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.TodoDao_Impl;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class NoteDatabase_Impl extends NoteDatabase {
  private volatile NoteDao _noteDao;

  private volatile TodoDao _todoDao;

  private volatile SingleTodoDao _singleTodoDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(2) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `note` (`mId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `mTitle` TEXT, `mDescription` TEXT, `mPriority` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `todo` (`mTodoId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `mTitleTodo` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `singleItemTodo` (`mSingleItemId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `mSingleItemTitle` TEXT, `mTodoSingleId` INTEGER NOT NULL, FOREIGN KEY(`mTodoSingleId`) REFERENCES `todo`(`mTodoId`) ON UPDATE NO ACTION ON DELETE CASCADE )");
        _db.execSQL("CREATE  INDEX `index_singleItemTodo_mSingleItemTitle` ON `singleItemTodo` (`mSingleItemTitle`)");
        _db.execSQL("CREATE  INDEX `index_singleItemTodo_mTodoSingleId` ON `singleItemTodo` (`mTodoSingleId`)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"5ab8a7a3f48d39780119b8e8ce4f1e78\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `note`");
        _db.execSQL("DROP TABLE IF EXISTS `todo`");
        _db.execSQL("DROP TABLE IF EXISTS `singleItemTodo`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        _db.execSQL("PRAGMA foreign_keys = ON");
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsNote = new HashMap<String, TableInfo.Column>(4);
        _columnsNote.put("mId", new TableInfo.Column("mId", "INTEGER", true, 1));
        _columnsNote.put("mTitle", new TableInfo.Column("mTitle", "TEXT", false, 0));
        _columnsNote.put("mDescription", new TableInfo.Column("mDescription", "TEXT", false, 0));
        _columnsNote.put("mPriority", new TableInfo.Column("mPriority", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysNote = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesNote = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoNote = new TableInfo("note", _columnsNote, _foreignKeysNote, _indicesNote);
        final TableInfo _existingNote = TableInfo.read(_db, "note");
        if (! _infoNote.equals(_existingNote)) {
          throw new IllegalStateException("Migration didn't properly handle note(com.sheepApps.android.iamaslov.TODO.model.Note_model.Note).\n"
                  + " Expected:\n" + _infoNote + "\n"
                  + " Found:\n" + _existingNote);
        }
        final HashMap<String, TableInfo.Column> _columnsTodo = new HashMap<String, TableInfo.Column>(2);
        _columnsTodo.put("mTodoId", new TableInfo.Column("mTodoId", "INTEGER", true, 1));
        _columnsTodo.put("mTitleTodo", new TableInfo.Column("mTitleTodo", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTodo = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTodo = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTodo = new TableInfo("todo", _columnsTodo, _foreignKeysTodo, _indicesTodo);
        final TableInfo _existingTodo = TableInfo.read(_db, "todo");
        if (! _infoTodo.equals(_existingTodo)) {
          throw new IllegalStateException("Migration didn't properly handle todo(com.sheepApps.android.iamaslov.TODO.model.TODO_model.Todo).\n"
                  + " Expected:\n" + _infoTodo + "\n"
                  + " Found:\n" + _existingTodo);
        }
        final HashMap<String, TableInfo.Column> _columnsSingleItemTodo = new HashMap<String, TableInfo.Column>(3);
        _columnsSingleItemTodo.put("mSingleItemId", new TableInfo.Column("mSingleItemId", "INTEGER", true, 1));
        _columnsSingleItemTodo.put("mSingleItemTitle", new TableInfo.Column("mSingleItemTitle", "TEXT", false, 0));
        _columnsSingleItemTodo.put("mTodoSingleId", new TableInfo.Column("mTodoSingleId", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysSingleItemTodo = new HashSet<TableInfo.ForeignKey>(1);
        _foreignKeysSingleItemTodo.add(new TableInfo.ForeignKey("todo", "CASCADE", "NO ACTION",Arrays.asList("mTodoSingleId"), Arrays.asList("mTodoId")));
        final HashSet<TableInfo.Index> _indicesSingleItemTodo = new HashSet<TableInfo.Index>(2);
        _indicesSingleItemTodo.add(new TableInfo.Index("index_singleItemTodo_mSingleItemTitle", false, Arrays.asList("mSingleItemTitle")));
        _indicesSingleItemTodo.add(new TableInfo.Index("index_singleItemTodo_mTodoSingleId", false, Arrays.asList("mTodoSingleId")));
        final TableInfo _infoSingleItemTodo = new TableInfo("singleItemTodo", _columnsSingleItemTodo, _foreignKeysSingleItemTodo, _indicesSingleItemTodo);
        final TableInfo _existingSingleItemTodo = TableInfo.read(_db, "singleItemTodo");
        if (! _infoSingleItemTodo.equals(_existingSingleItemTodo)) {
          throw new IllegalStateException("Migration didn't properly handle singleItemTodo(com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoItem).\n"
                  + " Expected:\n" + _infoSingleItemTodo + "\n"
                  + " Found:\n" + _existingSingleItemTodo);
        }
      }
    }, "5ab8a7a3f48d39780119b8e8ce4f1e78", "11f76f5e71d1419c4a991aa1342cc3a4");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "note","todo","singleItemTodo");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    boolean _supportsDeferForeignKeys = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP;
    try {
      if (!_supportsDeferForeignKeys) {
        _db.execSQL("PRAGMA foreign_keys = FALSE");
      }
      super.beginTransaction();
      if (_supportsDeferForeignKeys) {
        _db.execSQL("PRAGMA defer_foreign_keys = TRUE");
      }
      _db.execSQL("DELETE FROM `note`");
      _db.execSQL("DELETE FROM `todo`");
      _db.execSQL("DELETE FROM `singleItemTodo`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      if (!_supportsDeferForeignKeys) {
        _db.execSQL("PRAGMA foreign_keys = TRUE");
      }
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public NoteDao mNoteDao() {
    if (_noteDao != null) {
      return _noteDao;
    } else {
      synchronized(this) {
        if(_noteDao == null) {
          _noteDao = new NoteDao_Impl(this);
        }
        return _noteDao;
      }
    }
  }

  @Override
  public TodoDao mTodoDao() {
    if (_todoDao != null) {
      return _todoDao;
    } else {
      synchronized(this) {
        if(_todoDao == null) {
          _todoDao = new TodoDao_Impl(this);
        }
        return _todoDao;
      }
    }
  }

  @Override
  public SingleTodoDao mSingleTodoDao() {
    if (_singleTodoDao != null) {
      return _singleTodoDao;
    } else {
      synchronized(this) {
        if(_singleTodoDao == null) {
          _singleTodoDao = new SingleTodoDao_Impl(this);
        }
        return _singleTodoDao;
      }
    }
  }
}
