package com.sheepApps.android.iamaslov.TODO.model.Note_model;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface NoteDao {

    @Query("Select * from note Order by mPriority DESC")
    LiveData<List<Note>> getAllNotes();

    @Delete
    void delete(Note note);

    @Query("Delete from note ")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Note note);

    @Update
    void update(Note note);


}
