package com.sheepApps.android.iamaslov.TODO.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.sheepApps.android.iamaslov.TODO.model.Note_model.Note;
import com.sheepApps.android.iamaslov.TODO.model.Note_model.NoteDao;
import com.sheepApps.android.iamaslov.TODO.model.NoteDatabase;

import java.util.List;

import androidx.lifecycle.LiveData;

public class NoteRepository {
    private NoteDao mNoteDao;
    private LiveData<List<Note>> mAllNotes;

    public NoteRepository(Application application) {
        NoteDatabase database = NoteDatabase.getInstance(application);
        mNoteDao = database.mNoteDao();
        mAllNotes = mNoteDao.getAllNotes();
    }


    public LiveData<List<Note>> getAllNotes() {
        return mAllNotes;
    }


    public void insert(Note note) {
        new InsertAsyncTask(mNoteDao).execute(note);
    }

    private static class InsertAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDao mAsyncTaskNoteDao;

        public InsertAsyncTask(NoteDao noteDao) {
            mAsyncTaskNoteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            mAsyncTaskNoteDao.insert(notes[0]);
            return null;
        }
    }

    public void update(Note note) {
        new UpdateAsyncTask(mNoteDao).execute(note);

    }

    private static class UpdateAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDao mAsyncTaskNoteDao;

        public UpdateAsyncTask(NoteDao noteDao) {
            mAsyncTaskNoteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            mAsyncTaskNoteDao.update(notes[0]);
            return null;
        }
    }

    public void delete(Note note) {
        new DeleteAsyncTask(mNoteDao).execute(note);
    }

    private static class DeleteAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDao mAsyncTaskNoteDao;

        public DeleteAsyncTask(NoteDao noteDao) {
            mAsyncTaskNoteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            mAsyncTaskNoteDao.delete(notes[0]);
            return null;
        }
    }

    public void deleteAllNotes() {
        new DeleteAllNotesAsyncTask(mNoteDao).execute();
    }

    private static class DeleteAllNotesAsyncTask extends AsyncTask<Void, Void, Void> {
        private NoteDao mAsyncTaskNoteDao;

        public DeleteAllNotesAsyncTask(NoteDao noteDao) {
            mAsyncTaskNoteDao = noteDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskNoteDao.deleteAll();
            return null;
        }
    }
}
