package com.sheepApps.android.iamaslov.TODO.view.TODO_view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sheepApps.android.iamaslov.TODO.R;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.Todo;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoHolder> {
    private OnItemClickListener mListener;
    private LayoutInflater layoutInflater;
    private Context context;
    private List<Todo> mTodoList;

    public TodoAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setTodoList(List<Todo> todoList) {
        this.mTodoList = todoList;
        notifyDataSetChanged();
    }

    public Todo getTodoAt(int position) {
        return mTodoList.get(position);

    }


    @NonNull
    @Override
    public TodoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_task_item, parent, false);
        return new TodoHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoHolder holder, int position) {

        if (mTodoList == null) {
            return;
        }

        final Todo todo = mTodoList.get(position);
        if (todo != null) {
            holder.mTextViewTodoTitle.setText(todo.getTitleTodo());

        }

    }

    @Override
    public int getItemCount() {
        if (mTodoList == null) {
            return 0;
        } else {
            return mTodoList.size();
        }
    }


    class TodoHolder extends RecyclerView.ViewHolder {
        private TextView mTextViewTodoTitle;


        public TodoHolder(@NonNull View itemView) {
            super(itemView);
            mTextViewTodoTitle = itemView.findViewById(R.id.text_view_todo_title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mListener != null && position != RecyclerView.NO_POSITION) {
                        mListener.onItemClick(mTodoList.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Todo todo);
    }

    public void setOnItemClickListener(TodoAdapter.OnItemClickListener listener) {
        mListener = listener;
    }
}
