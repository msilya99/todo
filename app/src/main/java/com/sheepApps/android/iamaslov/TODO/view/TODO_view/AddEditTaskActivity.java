package com.sheepApps.android.iamaslov.TODO.view.TODO_view;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sheepApps.android.iamaslov.TODO.R;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoItem;
import com.sheepApps.android.iamaslov.TODO.view_model.SingleTodoViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AddEditTaskActivity extends AppCompatActivity {
    private static final String TAG = "TaskTodoDialog";
    private static final String TAG2 = "TaskSingleTodoDialog";
    public static final String EXTRA_TODO_TITLE = "com.sheepApps.android.iamaslov.TODO.view.EXTRA_TODO_TITLE";
    public static final String EXTRA_TODO_ID = "com.sheepApps.android.iamaslov.TODO.view.EXTRA_TODO_ID";
    public static final String TAG_DIALOG_SINGLE_TODO_SAVE = "single_todo_save";

    private SingleTodoViewModel mSingleTodoViewModel;
    private RecyclerView mRecyclerView;
    private int titleId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view_with_fb);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_with_fb);
        final SingleItemTodoAdapter singleItemTodoAdapter = new SingleItemTodoAdapter(getApplicationContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(singleItemTodoAdapter);
        mRecyclerView.setHasFixedSize(true);

        titleId = getIntent().getIntExtra(EXTRA_TODO_ID, -1);


        mSingleTodoViewModel = ViewModelProviders.of(this).get(SingleTodoViewModel.class);

        mSingleTodoViewModel.getAllSingleTodo().observe(this, new Observer<List<SingleTodoItem>>() {
            @Override
            public void onChanged(List<SingleTodoItem> singleTodoItems) {
                singleItemTodoAdapter.setSingleTodoItemListList(mSingleTodoViewModel.getSingleTodoById(titleId));


            }
        });


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                mSingleTodoViewModel.deleteSingleItem(singleItemTodoAdapter.getSingleTodoAt(viewHolder.getAdapterPosition()));
                Toast.makeText(AddEditTaskActivity.this, "This task has been deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(mRecyclerView);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_single_todo);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogFragment dialogFragment = SingleSaveDialog.newInstance(null, titleId);
                dialogFragment.show(getSupportFragmentManager(), TAG_DIALOG_SINGLE_TODO_SAVE);
            }
        });

        setTodoListTitle();


    }

    public void setTodoListTitle() {
        Intent intent = getIntent();
        setTitle(intent.getStringExtra(EXTRA_TODO_TITLE));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.close_todo_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.close:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
