package com.sheepApps.android.iamaslov.TODO.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.sheepApps.android.iamaslov.TODO.R;
import com.sheepApps.android.iamaslov.TODO.view.Note_view.AddEditNoteActivity;
import com.sheepApps.android.iamaslov.TODO.view.TODO_view.TodoSaveDialog;
import com.sheepApps.android.iamaslov.TODO.view.Warnings.WarningNote;
import com.sheepApps.android.iamaslov.TODO.view.Warnings.WarningTaskList;
import com.sheepApps.android.iamaslov.TODO.view_model.NoteViewModel;
import com.sheepApps.android.iamaslov.TODO.view_model.TodoViewModel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import static com.sheepApps.android.iamaslov.TODO.view.TODO_view.SingleSaveDialog.TAG_DIALOG_SINGLE_TODO_SAVE;
import static com.sheepApps.android.iamaslov.TODO.view.TabActivity.REQUEST_ADD_NOTE;
import static com.sheepApps.android.iamaslov.TODO.view.Warnings.WarningNote.TAG_DIALOG_NOTE_DELETE_ALL;
import static com.sheepApps.android.iamaslov.TODO.view.Warnings.WarningTaskList.TAG_DIALOG_TODO_DELETE_ALL;

public class BottomSheetDialog extends BottomSheetDialogFragment {
    private static final String TAG1 = "BottomSheetDialog";
    private BottomSheetListener mListener;
    private Button mButton1;
    private Button mButton2;
    private Button mButtonDeleteAllTodo;
    private Button mButtonDeleteAllNotes;
    private NoteViewModel mNoteViewModel;
    private TodoViewModel mTodoViewModel;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_layout, container, false);
        mNoteViewModel = ViewModelProviders.of(getActivity()).get(NoteViewModel.class);
        mTodoViewModel = ViewModelProviders.of(getActivity()).get(TodoViewModel.class);
        mButton1 = v.findViewById(R.id.btn_add_note);
        mButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddEditNoteActivity.class);
                getActivity().startActivityForResult(intent, REQUEST_ADD_NOTE);
                dismiss();
            }
        });

        mButton2 = v.findViewById(R.id.btn_add_task);
        mButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                DialogFragment dialogFragment = TodoSaveDialog.newInstance(null);
                dialogFragment.show(manager, TAG_DIALOG_SINGLE_TODO_SAVE);

                dismiss();
            }
        });


        mButtonDeleteAllNotes = v.findViewById(R.id.btn_delete_all_notes);
        mButtonDeleteAllNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                DialogFragment dialogFragment = WarningNote.newInstance();
                dialogFragment.show(manager, TAG_DIALOG_NOTE_DELETE_ALL);

                dismiss();
            }
        });

        mButtonDeleteAllTodo = v.findViewById(R.id.btn_delete_all_todos);
        mButtonDeleteAllTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                DialogFragment dialogFragment = WarningTaskList.newInstance();
                dialogFragment.show(manager, TAG_DIALOG_TODO_DELETE_ALL);

                dismiss();

            }
        });


        return v;
    }


    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (BottomSheetListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement BottomSheetListener");
        }

    }
}

