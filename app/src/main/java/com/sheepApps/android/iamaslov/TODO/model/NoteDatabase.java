package com.sheepApps.android.iamaslov.TODO.model;

import android.content.Context;
import android.os.AsyncTask;

import com.sheepApps.android.iamaslov.TODO.model.Note_model.Note;
import com.sheepApps.android.iamaslov.TODO.model.Note_model.NoteDao;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoDao;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoItem;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.Todo;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.TodoDao;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {Note.class, Todo.class, SingleTodoItem.class}, version = 2, exportSchema = false)
public abstract class NoteDatabase extends RoomDatabase {
    public abstract NoteDao mNoteDao();

    public abstract TodoDao mTodoDao();

    public abstract SingleTodoDao mSingleTodoDao();

    private static NoteDatabase instance;

    public static synchronized NoteDatabase getInstance(Context context) {

        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    NoteDatabase.class, "note_database")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .addCallback(sCallback)
                    .build();

        }
        return instance;
    }

    private static RoomDatabase.Callback sCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopylateDbAsyncTask(instance).execute();
        }
    };

    private static class PopylateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private final NoteDao mNoteDao;
        private final TodoDao mTodoDao;
        private final SingleTodoDao mSingleTodoDao;


        public PopylateDbAsyncTask(NoteDatabase db) {
            mNoteDao = db.mNoteDao();
            mTodoDao = db.mTodoDao();
            mSingleTodoDao = db.mSingleTodoDao();
        }

        @Override
        protected Void doInBackground(final Void... voids) {
            mNoteDao.insert(new Note("Title1", "Description1", 1));
            mNoteDao.insert(new Note("Title3", "Description2", 6));
            mNoteDao.insert(new Note("Title4", "Description56", 4));
            mNoteDao.insert(new Note("Title5", "Description3", 3));

            mTodoDao.insert(new Todo("List1"));
            mTodoDao.insert(new Todo("Shoping"));
            mTodoDao.insert(new Todo("Work project"));


            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Task1", 1));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Task2", 1));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Task3", 1));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Task4", 1));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Task5", 1));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Task6", 1));

            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Milk", 2));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Watter", 2));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Bread", 2));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Coffee", 2));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Eggs", 2));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Meat", 2));

            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Install todo", 3));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Open todo", 3));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Check todo", 3));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Find this list", 3));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Smile", 3));
            mSingleTodoDao.insertSingleItem(new SingleTodoItem("Close", 3));

            return null;

        }
    }


}
