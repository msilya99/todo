package com.sheepApps.android.iamaslov.TODO.view.TODO_view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sheepApps.android.iamaslov.TODO.R;
import com.sheepApps.android.iamaslov.TODO.model.NoteDatabase;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoDao;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;

public class SingleSaveDialog extends AppCompatDialogFragment {

    private Context context;
    private String singleTodoTitleExtra;
    private int todoIdExtra;
    private static final String TAG = "SingleSaveDialog";

    private static final String EXTRA_SINGLE_TODO_TITLE = "single_todo_title";
    private static final String EXTRA_TODO_ID = "todo_id";
    public static final String TAG_DIALOG_SINGLE_TODO_SAVE = "single_todo_save";

    public static SingleSaveDialog newInstance(final String singleTodoTitle, final int todo_id) {
        SingleSaveDialog fragment = new SingleSaveDialog();

        Bundle args = new Bundle();
        args.putString(EXTRA_SINGLE_TODO_TITLE, singleTodoTitle);
        args.putInt(EXTRA_TODO_ID, todo_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        singleTodoTitleExtra = args.getString(EXTRA_SINGLE_TODO_TITLE);
        todoIdExtra = args.getInt(EXTRA_TODO_ID);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_single_title, null);
        final EditText singleTodoEditText = view.findViewById(R.id.etSingleTodoTitle);
        if (singleTodoTitleExtra != null) {
            singleTodoEditText.setText(singleTodoTitleExtra);
            singleTodoEditText.setSelection(singleTodoTitleExtra.length());

        }


        alertDialogBuilder.setView(view)
                .setTitle(getString(R.string.dialog_single_todo_title))
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveSigleTodo(singleTodoEditText.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        return alertDialogBuilder.create();
    }

    private void saveSigleTodo(String singleTodoTitle) {

        if (TextUtils.isEmpty(singleTodoTitle)) {
            Toast.makeText(context, "Plz enter your task", Toast.LENGTH_SHORT).show();
            return;
        }
        SingleTodoDao singleTodoDao = NoteDatabase.getInstance(context).mSingleTodoDao();
        singleTodoDao.insertSingleItem(new SingleTodoItem(singleTodoTitle, todoIdExtra));

    }
}
