package com.sheepApps.android.iamaslov.TODO.model.TODO_model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "todo")
public class Todo {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "mTodoId")
    private int mTodoId;

    private String mTitleTodo;


    public void setTitleTodo(String titleTodo) {
        mTitleTodo = titleTodo;
    }

    public Todo(String titleTodo) {
        mTitleTodo = titleTodo;
    }

    public int getTodoId() {
        return mTodoId;
    }

    public String getTitleTodo() {
        return mTitleTodo;
    }

    public void setTodoId(int todoId) {
        mTodoId = todoId;
    }
}
