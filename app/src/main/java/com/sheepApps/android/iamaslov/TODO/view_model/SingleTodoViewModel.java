package com.sheepApps.android.iamaslov.TODO.view_model;

import android.app.Application;

import com.sheepApps.android.iamaslov.TODO.model.NoteDatabase;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoDao;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoItem;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class SingleTodoViewModel extends AndroidViewModel {
    private LiveData<List<SingleTodoItem>> mAllSingleTodo;
    private SingleTodoDao mSingleTodoDao;

    public SingleTodoViewModel(Application application) {
        super(application);
        mSingleTodoDao = NoteDatabase.getInstance(application).mSingleTodoDao();
        mAllSingleTodo = mSingleTodoDao.getAllSingleTodo();
    }


    public LiveData<List<SingleTodoItem>> getAllSingleTodo() {
        return mAllSingleTodo;
    }

    public List<SingleTodoItem> getSingleTodoById(int id) {
        return mSingleTodoDao.findSingleTodoById(id);
    }

    public void insertSingleItem(SingleTodoItem singleTodoItem) {
        mSingleTodoDao.insertSingleItem(singleTodoItem);
    }

    public void insertSingleItem(SingleTodoItem... singleTodoItem) {
        mSingleTodoDao.insertSingleItem(singleTodoItem);
    }

    public void updateSingleItem(SingleTodoItem singleTodoItem) {
        mSingleTodoDao.updateSingleItem(singleTodoItem);
    }

    public void deleteSingleItem(SingleTodoItem singleTodoItem) {
        mSingleTodoDao.deleteSingleItem(singleTodoItem);
    }

    public void deleteAllSingleItems() {
        mSingleTodoDao.deleteAllSingleItems();
    }
}

