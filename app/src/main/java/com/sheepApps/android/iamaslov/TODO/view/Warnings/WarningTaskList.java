package com.sheepApps.android.iamaslov.TODO.view.Warnings;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sheepApps.android.iamaslov.TODO.R;
import com.sheepApps.android.iamaslov.TODO.view_model.TodoViewModel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.lifecycle.ViewModelProviders;

public class WarningTaskList extends AppCompatDialogFragment {

    private Context context;
    private TodoViewModel mTodoViewModel;
    public static final String TAG_DIALOG_TODO_DELETE_ALL = "todo_delete_all";


    public static WarningTaskList newInstance() {
        WarningTaskList fragment = new WarningTaskList();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mTodoViewModel = ViewModelProviders.of(getActivity()).get(TodoViewModel.class);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.warning_note_delete, null);
        TextView warningTaskTextView = view.findViewById(R.id.warning_view);
        warningTaskTextView.setText(R.string.todo_delete_warning);

        alertDialogBuilder.setView(view)
                .setTitle(getString(R.string.warning))
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mTodoViewModel.deleteAll();
                        Toast.makeText(context, "All tasks has been deleted", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        return alertDialogBuilder.create();
    }


}

