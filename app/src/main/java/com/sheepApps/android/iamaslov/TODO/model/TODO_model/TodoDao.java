package com.sheepApps.android.iamaslov.TODO.model.TODO_model;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;


@Dao
public interface TodoDao {

    @Query("SELECT * FROM todo WHERE mTodoId = :id LIMIT 1")
    Todo findTodoById(int id);

    @Query("SELECT * FROM todo WHERE mTitleTodo = :mTitleTodo LIMIT 1")
    Todo findTodoByName(String mTitleTodo);

    @Query("Select * from todo Order by mTodoId ASC")
    LiveData<List<Todo>> getAllTodo();

    @Query("SELECT MAX(mTodoId) FROM todo ")
    int findLastId();

    @Query("Delete from todo ")
    void deleteAll();

    @Delete
    void delete(Todo todo);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Todo todo);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Todo... todo);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(Todo todo);


}
