package com.sheepApps.android.iamaslov.TODO.model.TODO_model;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;


@Dao
public interface SingleTodoDao {

    @Query("Select * from singleItemTodo Order by mSingleItemId ASC ")
    LiveData<List<SingleTodoItem>> getAllSingleTodo();

    @Query("SELECT * FROM singleItemTodo WHERE mSingleItemTitle = :title ")
    SingleTodoItem findSingleTodoByTitle(String title);

    @Query("SELECT * FROM singleItemTodo WHERE mTodoSingleId = :todoId ")
    List<SingleTodoItem> findSingleTodoById(int todoId);

    @Delete
    void deleteSingleItem(SingleTodoItem singleTodoItem);

    @Query("Delete from singleItemTodo ")
    void deleteAllSingleItems();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertSingleItem(SingleTodoItem singleTodoItem);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertSingleItem(SingleTodoItem... singleTodoItem);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateSingleItem(SingleTodoItem singleTodoItem);
}