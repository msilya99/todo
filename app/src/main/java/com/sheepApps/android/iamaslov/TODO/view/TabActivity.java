package com.sheepApps.android.iamaslov.TODO.view;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.sheepApps.android.iamaslov.TODO.R;
import com.sheepApps.android.iamaslov.TODO.model.Note_model.Note;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.Todo;
import com.sheepApps.android.iamaslov.TODO.view.Note_view.AddEditNoteActivity;
import com.sheepApps.android.iamaslov.TODO.view.Note_view.Tabs1Frgmnt;
import com.sheepApps.android.iamaslov.TODO.view.TODO_view.Tabs2Frgmnt;
import com.sheepApps.android.iamaslov.TODO.view_model.NoteViewModel;
import com.sheepApps.android.iamaslov.TODO.view_model.TodoViewModel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static com.sheepApps.android.iamaslov.TODO.view.Note_view.AddEditNoteActivity.EXTRA_DESCRIPTION;
import static com.sheepApps.android.iamaslov.TODO.view.Note_view.AddEditNoteActivity.EXTRA_PRIORITY;
import static com.sheepApps.android.iamaslov.TODO.view.Note_view.AddEditNoteActivity.EXTRA_TITLE;
import static com.sheepApps.android.iamaslov.TODO.view.TODO_view.AddEditTaskActivity.EXTRA_TODO_TITLE;


public class TabActivity extends AppCompatActivity
        implements BottomSheetDialog.BottomSheetListener {
    private static final String TAG = "TabActivity";
    public static final int REQUEST_ADD_NOTE = 1;
    public static final int REQUEST_EDIT_NOTE = 2;
    public static final int REQUEST_ADD_TODO = 3;
    public static final int REQUEST_EDIT_TODO = 4;


    private SectionPagerAdapter mSectionPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private NoteViewModel mNoteViewModel;
    private TodoViewModel mTodoViewModel;
    private RecyclerView mRecyclerView;
    private List<Note> mNotes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);


        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        mNoteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        mTodoViewModel = ViewModelProviders.of(this).get(TodoViewModel.class);


        Log.d(TAG, "onCreate: Starting");

        mSectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog();
                bottomSheetDialog.show(getSupportFragmentManager(), "BottomSheetDialog");

            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        SectionPagerAdapter adapter = new SectionPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Tabs1Frgmnt(), "Notes");
        adapter.addFragment(new Tabs2Frgmnt(), "Todo");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onButtonClicked(String text) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ADD_NOTE && resultCode == RESULT_OK) {
            String title = data.getStringExtra(EXTRA_TITLE);
            String description = data.getStringExtra(EXTRA_DESCRIPTION);
            int priority = data.getIntExtra(EXTRA_PRIORITY, 1);

            Note note = new Note(title, description, priority);
            mNoteViewModel.insert(note);

            Toast.makeText(this, "Note Saved", Toast.LENGTH_SHORT).show();
        } else if (requestCode == REQUEST_EDIT_NOTE && resultCode == RESULT_OK) {
            int id = data.getIntExtra(AddEditNoteActivity.EXTRA_ID, -1);

            if (id == -1) {
                Toast.makeText(this, "Note can't be updated", Toast.LENGTH_SHORT).show();
                return;
            }

            String title = data.getStringExtra(EXTRA_TITLE);
            String description = data.getStringExtra(EXTRA_DESCRIPTION);
            int priority = data.getIntExtra(EXTRA_PRIORITY, 1);

            Note note = new Note(title, description, priority);
            note.setId(id);
            mNoteViewModel.update(note);
            Toast.makeText(this, "Note have been updated", Toast.LENGTH_SHORT).show();
        }
        if (requestCode == REQUEST_ADD_TODO && resultCode == RESULT_OK) {
            String titleTodo = data.getStringExtra(EXTRA_TODO_TITLE);
            Todo todo = new Todo(titleTodo);
            mTodoViewModel.insert(todo);

            Toast.makeText(this, "Todo Saved", Toast.LENGTH_SHORT).show();
        }
    }


}
