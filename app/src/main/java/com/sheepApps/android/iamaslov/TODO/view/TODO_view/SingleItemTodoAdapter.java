package com.sheepApps.android.iamaslov.TODO.view.TODO_view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sheepApps.android.iamaslov.TODO.R;
import com.sheepApps.android.iamaslov.TODO.model.NoteDatabase;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.SingleTodoItem;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SingleItemTodoAdapter extends RecyclerView.Adapter<SingleItemTodoAdapter.SingleTodoHolder> {
    private LayoutInflater layoutInflater;
    private List<SingleTodoItem> mSingleTodoItemListList;
    private Context context;

    public SingleItemTodoAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setSingleTodoItemListList(List<SingleTodoItem> singleTodoItemListList) {
        this.mSingleTodoItemListList = singleTodoItemListList;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public SingleTodoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_of_tasks, parent, false);
        return new SingleTodoHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SingleTodoHolder holder, int position) {
        if (mSingleTodoItemListList == null) {
            return;
        }

        final SingleTodoItem singleTodoItem = mSingleTodoItemListList.get(position);
        if (singleTodoItem != null) {
            holder.titleText.setText(singleTodoItem.getSingleItemTitle());

            final SingleTodoItem singleTodoItem1 = NoteDatabase.getInstance(context).mSingleTodoDao().findSingleTodoByTitle(singleTodoItem.getSingleItemTitle());
            holder.titleText.setText(singleTodoItem1.getSingleItemTitle());

        }

    }

    @Override
    public int getItemCount() {
        if (mSingleTodoItemListList == null) {
            return 0;
        } else {
            return mSingleTodoItemListList.size();
        }
    }

    public SingleTodoItem getSingleTodoAt(int position) {
        return mSingleTodoItemListList.get(position);

    }


    class SingleTodoHolder extends RecyclerView.ViewHolder {
        private TextView titleText;


        public SingleTodoHolder(@NonNull View itemView) {
            super(itemView);
            titleText = itemView.findViewById(R.id.textview_single_task);


        }
    }
}
