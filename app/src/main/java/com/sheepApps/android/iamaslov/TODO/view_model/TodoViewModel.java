package com.sheepApps.android.iamaslov.TODO.view_model;

import android.app.Application;

import com.sheepApps.android.iamaslov.TODO.model.NoteDatabase;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.Todo;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.TodoDao;


import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class TodoViewModel extends AndroidViewModel {

    private LiveData<List<Todo>> mAllTodo;
    private TodoDao mTodoDao;

    public TodoViewModel(Application application) {
        super(application);
        mTodoDao = NoteDatabase.getInstance(application).mTodoDao();
        mAllTodo = mTodoDao.getAllTodo();

    }

    public LiveData<List<Todo>> getAllTodo() {
        return mAllTodo;
    }


    public void insert(Todo todo) {
        mTodoDao.insert(todo);
    }

    public void insert(Todo... todo) {
        mTodoDao.insert(todo);
    }


    public void update(Todo todo) {
        mTodoDao.update(todo);
    }

    public void delete(Todo todo) {
        mTodoDao.delete(todo);
    }

    public void deleteAll() {
        mTodoDao.deleteAll();
    }

    public int findLastId() {
        return mTodoDao.findLastId() + 1;
    }
}
