package com.sheepApps.android.iamaslov.TODO.view.TODO_view;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sheepApps.android.iamaslov.TODO.R;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.Todo;
import com.sheepApps.android.iamaslov.TODO.view_model.TodoViewModel;

import java.util.List;

import static com.sheepApps.android.iamaslov.TODO.view.TabActivity.REQUEST_EDIT_TODO;

public class Tabs2Frgmnt extends Fragment {
    private RecyclerView mRecyclerView;
    private TodoViewModel mTodoViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.recycler_view, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        final TodoAdapter todoAdapter = new TodoAdapter(getContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(todoAdapter);

        mTodoViewModel = ViewModelProviders.of(getActivity()).get(TodoViewModel.class);
        mTodoViewModel.getAllTodo().observe(this, new Observer<List<Todo>>() {
            @Override
            public void onChanged(List<Todo> todos) {
                todoAdapter.setTodoList(todos);
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                mTodoViewModel.delete(todoAdapter.getTodoAt(viewHolder.getAdapterPosition()));
                Toast.makeText(getActivity(), "Note Deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(mRecyclerView);

        todoAdapter.setOnItemClickListener(new TodoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Todo todo) {
                Intent intent = new Intent(getActivity(), AddEditTaskActivity.class);
                intent.putExtra(AddEditTaskActivity.EXTRA_TODO_ID, todo.getTodoId());
                intent.putExtra(AddEditTaskActivity.EXTRA_TODO_TITLE, todo.getTitleTodo());
                getActivity().startActivityForResult(intent, REQUEST_EDIT_TODO);

            }
        });

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
}
