package com.sheepApps.android.iamaslov.TODO.model.Note_model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "note")
public class Note {

    @PrimaryKey(autoGenerate = true)
    private int mId;
    private String mTitle;
    private String mDescription;
    private int mPriority;

    public Note(String title, String description, int priority) {
        mTitle = title;
        mDescription = description;
        mPriority = priority;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getPriority() {
        return mPriority;
    }
}
