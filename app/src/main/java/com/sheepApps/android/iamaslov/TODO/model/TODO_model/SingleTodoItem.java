package com.sheepApps.android.iamaslov.TODO.model.TODO_model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;


@Entity(tableName = "singleItemTodo",
        foreignKeys = @ForeignKey(entity = Todo.class,
                parentColumns = "mTodoId",
                childColumns = "mTodoSingleId",
                onDelete = ForeignKey.CASCADE),
        indices = {@Index("mSingleItemTitle"), @Index("mTodoSingleId")})
public class SingleTodoItem {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "mSingleItemId")
    private int mSingleItemId;

    @ColumnInfo(name = "mSingleItemTitle")
    private String mSingleItemTitle;

    @ColumnInfo(name = "mTodoSingleId")
    private int mTodoSingleId;

    public void setSingleItemTitle(String singleItemTitle) {
        mSingleItemTitle = singleItemTitle;
    }

    public void setTodoSingleId(int todoSingleId) {
        mTodoSingleId = todoSingleId;
    }

    public SingleTodoItem(String singleItemTitle, int todoSingleId) {
        mSingleItemTitle = singleItemTitle;
        mTodoSingleId = todoSingleId;
    }


    public int getSingleItemId() {
        return mSingleItemId;
    }

    public String getSingleItemTitle() {
        return mSingleItemTitle;
    }

    public int getTodoSingleId() {
        return mTodoSingleId;
    }

    public void setSingleItemId(int singleItemId) {
        mSingleItemId = singleItemId;
    }
}
