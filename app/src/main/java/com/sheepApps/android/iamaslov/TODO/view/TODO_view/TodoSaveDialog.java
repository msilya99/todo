package com.sheepApps.android.iamaslov.TODO.view.TODO_view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sheepApps.android.iamaslov.TODO.R;
import com.sheepApps.android.iamaslov.TODO.model.NoteDatabase;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.Todo;
import com.sheepApps.android.iamaslov.TODO.model.TODO_model.TodoDao;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;

public class TodoSaveDialog extends AppCompatDialogFragment {

    private Context context;
    private String mTodoTitle;

    private static final String EXTRA_TODO_TITLE = "todo_title";
    public static final String TAG_TODO_SAVE = "todo_save";

    public static TodoSaveDialog newInstance(String todoTitle) {
        TodoSaveDialog fragment = new TodoSaveDialog();

        Bundle args = new Bundle();
        args.putString(EXTRA_TODO_TITLE, todoTitle);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mTodoTitle = args.getString(EXTRA_TODO_TITLE);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog, null);
        final EditText todoEditText = view.findViewById(R.id.edit_todo);
        final TextView todoTextView = view.findViewById(R.id.pretitle_view);
        todoTextView.setText(R.string.pretitle_todo_list);
        if (mTodoTitle != null) {
            todoEditText.setText(mTodoTitle);
            todoEditText.setSelection(mTodoTitle.length());
        }

        alertDialogBuilder.setView(view)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveTodo(todoEditText.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        return alertDialogBuilder.create();
    }

    private void saveTodo(String title) {
        if (TextUtils.isEmpty(title)) {
            Toast.makeText(context, "Please fill your task list title", Toast.LENGTH_SHORT).show();
            return;
        }

        TodoDao todoDao = NoteDatabase.getInstance(context).mTodoDao();
        Todo todo = new Todo(title);
        todoDao.insert(todo);

    }
}
