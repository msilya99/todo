package com.sheepApps.android.iamaslov.TODO.view.Warnings;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sheepApps.android.iamaslov.TODO.R;
import com.sheepApps.android.iamaslov.TODO.view_model.NoteViewModel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.lifecycle.ViewModelProviders;

public class WarningNote extends AppCompatDialogFragment {

    private Context context;
    private NoteViewModel mNoteViewModel;
    public static final String TAG_DIALOG_NOTE_DELETE_ALL = "note_delete_all";


    public static WarningNote newInstance() {
        WarningNote fragment = new WarningNote();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mNoteViewModel = ViewModelProviders.of(getActivity()).get(NoteViewModel.class);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.warning_note_delete, null);
        TextView warningTaskTextView = view.findViewById(R.id.warning_view);
        warningTaskTextView.setText(R.string.note_delete_warning);

        alertDialogBuilder.setView(view)
                .setTitle(getString(R.string.warning))
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mNoteViewModel.deleteAll();
                        Toast.makeText(getActivity(), "All notes has been deleted", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        return alertDialogBuilder.create();
    }


}

